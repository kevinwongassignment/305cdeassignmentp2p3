/*global angular  */

var app = angular.module('app', ['ngRoute', 'ngCookies']);

app.controller('search', function($scope, $http, $cookies) {
    if($cookies.get("user") != undefined){
        $scope.loginInfo = "Hello, " + $cookies.get("user")
    } else{
       $scope.loginInfo = "You not yet logged in"
    }
    
    console.log($cookies.get("user") + ", you are " + $cookies.get("loginStatus"))
    $scope.message = 'Search'
    $scope.search = function($event) {
        if ($event.which == 13) { // enter key presses
            var search = $scope.searchTerm
            if (search.length != 0){
                var url = 'https://cde305-assignmentp2p3-kevinwong.c9users.io/search?title='+ search
                $http.get(url).then(function(response) {
                    $scope.movies = response.data.movies
                })
            } else {
                alert("You must input the keywords");
            }
        }
    }
    
    $scope.fav = function(id){
        console.log("fav")
        var user = $cookies.get("user")
        if (user != undefined && $cookies.get("loginStatus") != undefined){
            var url = 'https://cde305-assignmentp2p3-kevinwong.c9users.io/add/favourite?imdbId=' + id + "&userId=" + user
            $http.get(url).then(function(response){
                alert(response.data)
            })
        } else {
            alert("Please login first")
        }
    }
})

app.controller('register', function($scope, $http, $cookies){
    if($cookies.get("user") != undefined){
        $scope.loginInfo = "Hello, " + $cookies.get("user")
    } else{
       $scope.loginInfo = "You not yet logged in"
    }
    console.log($cookies.get("user") + ", you are " + $cookies.get("loginStatus"))
    $scope.message = 'Register Form'
    $scope.register = function($user) {
        if($scope.user.username.length != 0 && $scope.user.realname.length != 0 && $scope.user.password.length != 0 && $scope.user.email.length != 0){
            var userNameValid = checkuserName($scope.user.username);
	        var realNameValid = checkNameContainNumber($scope.user.realname);
	        var passwordValid = checkPassword($scope.user.password);
	        var emailValid = checkEmail($scope.user.email);
	        
    	    if(userNameValid == true && realNameValid == true && passwordValid == true && emailValid){
    	        var data = {
                    "username": $scope.user.username,
                    "realname": $scope.user.realname,
                    "password": $scope.user.password,
                    "email": $scope.user.email
                }
                console.log(data)
                var url = "https://cde305-assignmentp2p3-kevinwong.c9users.io/create"
                
                $http.post(url, data).then(function(response){
                    $scope.regStatus = response.data
                })
    	    } else {
    	        alert("Please input the correct information")
    	    }
        } else {
            alert("Please input all field");
        }
    }
})

app.controller('login', function($scope, $http, $cookies){
    if($cookies.get("user") != undefined){
        alert("you have logged in")
        window.location= "index.html"
    } else{
       $scope.loginInfo = "You not yet logged in"
    }
    console.log($cookies.get("user") + ", you are " + $cookies.get("loginStatus"))
    $scope.message = 'Login'
    $scope.login = function($user){
        if($scope.user.username.length != 0 && $scope.user.password.length != 0){
            var account = {
                "username": $scope.user.username,
                "password": $scope.user.password
            }
            console.log(account)
            var url = "https://cde305-assignmentp2p3-kevinwong.c9users.io/login"
            $http.post(url, account).then(function(response){
                $scope.loginStatus = response.data
                $cookies.put("user", account.username)
                $cookies.put("loginStatus", "loggedIn")
                console.log($cookies.get("user") + ", you are " + $cookies.get("loginStatus"))
            })
        } else {
            alert("Please input all field")
        }
    }
})

app.controller('logout', function($scope, $http, $cookies){
    $cookies.remove("user")
    $cookies.remove("loginStatus")
    alert("You logged out.")
    window.location = "index.html"
})

app.controller('favouriteList', function($scope, $http, $cookies){
   if($cookies.get("user") != undefined){
       $scope.loginInfo = "Hello, " + $cookies.get("user")
    } else{
       alert("Please login to access this function")
    window.location= "index.html"
    }
    var userId = $cookies.get("user");
    var url = 'https://cde305-assignmentp2p3-kevinwong.c9users.io/favourite?userId='+ userId
        $http.get(url).then(function(response) {
            $scope.list = response.data.list
        })
})

//Check user name
function checkuserName(username){
	//Check user name is less than 3 charator or number or empty
	if(username.length == 0 || username.length < 3){
		//Show error message
		return false;
	} else {
		//Turn to true
		return true;
	}
}

//Check name
function checkNameContainNumber(realName){
	//Create Regular expression which is only contain A-Z (a-z) and blank space
	var matches = /^[a-zA-Z\s]*$/;
		
		//Check either the name is valid
		if(matches.test(realName) && realName != ""){
			//Turn to true
			return true;
		} else {
			//Show error message
			return false;
		}
}

//Check e-mail
function checkEmail(email){
	//Create regular expression for e-mail format
	var matches = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	
		//Check either the e-mail is valid
		if(matches.test(email) && email != ""){
			//Turn to true
			return true;
		} else {
			//Show error message
			return false;
		}
}

//Check password
function checkPassword(pw){
	//Check password length is more than 8
	if(pw.length < 8){
		//Show error message
		return false
	} else{
		//Turn to true
		return true;
	}
}