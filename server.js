//Thing(s) to require
var request = require('request');
var syncRequest = require('sync-request');
var utf8 = require('utf8');
var urlLink = require('url');
var firebase = require('firebase');
var restify = require("restify");

//Third party API url
var omdburl = 'http://www.omdbapi.com/';
var youtubeUrl = 'https://www.googleapis.com/youtube/v3/search';

//Initialize the firebase app
firebase.initializeApp({
    serviceAccount: "assignmentp2p3-8203938203ba.json",
    databaseURL: "https://assignmentp2p3.firebaseio.com"
});

//Create server
var server = restify.createServer();

server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser());
server.use(restify.CORS({ credentials: true }));

//search movie routing
server.get("/search", searchMovieByTitle);
//get movie reviews routing
server.get("/reviews", getReviews);
//get favourities list
server.get("/favourite", getFavouritiesList);
//create user routing
server.post("/create", createUser);
//Add review routing
server.post("/add/review/:movie", addReview);
//Add movie to favourite list routing
server.get("/add/favourite", addToFavourite);
//User login
server.post("/login", userlogin);

function searchMovieByTitle(req, res, next) {
    console.log("Search");
    var query_string = {s: utf8.encode(req.query.title), type: "movie", r: "json"};
    
    request.get({url: omdburl, qs: query_string}, function(error, response, body){
        if (error) {
            console.log(error);
        } else {
            var movieInfoCombinedList = []; 
            var jsonData = JSON.parse(body);
            var movieList = jsonData.Search;
            
            for (var i = 0; i < movieList.length; i++) {
                
                var des = syncRequest('GET', omdburl, {
                    'qs':{
                        'i': movieList[i].imdbID,
                        'plot': "short",
                        'r': "json"
                    }
                });
                
                var desData = JSON.parse(des.getBody('utf8'));
                var movieDes = desData.Plot;
                
                var trailer = syncRequest('GET', youtubeUrl, {
                    'qs':{
                        'part': "snippet",
                        'maxResults': 1,
                        'q': utf8.encode(movieList[i].Title + " Trailer"),
                        'key': "AIzaSyD_m0tXKc2PlVI8dUo8ywT6FiVcHcteu1o"
                    }
                })
                
                var trailerData = JSON.parse(trailer.getBody('utf8'));
                var snippet = trailerData.items[0].snippet;
                var id = trailerData.items[0].id;
                
                var db = firebase.database();
                var reviewsRef = db.ref("review");
                var imdbIdRef = reviewsRef.child(movieList[i].imdbID);
                var comments;
                
                imdbIdRef.once("value", function(snapshot) {
                   comments = snapshot.val()
                });
                
                movieInfoCombinedList.push({
                                            'title': movieList[i].Title,
                                            'poster': movieList[i].Poster,
                                            'imdbID': movieList[i].imdbID,
                                            'des': movieDes,
                                            'trailerTitle': snippet.title,
                                            'videoId': id.videoId,
                                            'comments': comments
                                        });
            }
            res.setHeader("content-type", "application/json");
            res.send(200, {"movies": movieInfoCombinedList});
            res.end();
        }
    });
}

function createUser(req, res, next){
    console.log("create user");
    //get form data
    var userName = req.params.username;
    var realName = req.params.realname;
    var password = req.params.password;
    var email = req.params.email;
    console.log("" + userName +" "+ realName+ " " + password + " " + email);
	//create user into firebase
    
    var ref = firebase.database().ref("users");
        ref.once("value")
        .then(function(snapshot) {
        console.log(snapshot.val());
        var exist = snapshot.child(userName).exists();
        console.log(exist);
        if(exist == true) {
            res.send(200, "User name exist, please input another user name");
            res.end();
        } else {
            var db = firebase.database();
            var usersRef = db.ref("users");
            var userIdRef = usersRef.child(userName);
                userIdRef.set({
                    username: userName,
                    realname: realName,
                    password: password,
                    email: email
                });
                
            res.send(200, "Register success");
            res.end();
        }
    });
}

function userlogin(req, res, next){
    console.log("login");
    var userId = req.params.username;
    var password = req.params.password;
    
    var ref = firebase.database().ref("users");
        ref.once("value")
        .then(function(snapshot) {
        console.log(snapshot.val());
        var exist = snapshot.child(userId).exists();
        console.log(exist);
        if(exist != true) {
            res.send(200, "User not found, make sure you input the correct information");
            res.end();
        } else {
            var ref = firebase.database().ref("users/"+ userId);
            ref.once("value")
            .then(function(snapshot) {
                var userPassword = snapshot.child("password").val();
                if (password === userPassword){
                    res.send(200, "Hello, you have logged in as " + userId);
                    res.end();
                } else {
                    res.send(200, "Invalid password or ID");
                    res.end();
                }
            });
        }
    });
}
    
function addReview(req, res, next){
    var imdbId = req.params.imdbId;
    var userId = req.params.userId;
    var comment = req.params.comment;
    var db = firebase.database();
    var ref = db.ref("review");
    var movieIdRef = ref.child(imdbId);
    var userIdRef = movieIdRef.child(userId);
    
    userIdRef.push().set({
            content: comment
    });
    
    res.send(200, "Review is added");
    res.end()
}

function getReviews(req, res, next){
    var imdbId = req.params.imdbId = "tt762112";
    var db = firebase.database();
    var reviewsRef = db.ref("review");
    var imdbIdRef = reviewsRef.child(imdbId);
    
    imdbIdRef.once("value", function(snapshot) {
        res.json({
           result: snapshot.val()
        });
    });
}

function addToFavourite(req, res, next){
    console.log("Fav")
    var imdbId = req.query.imdbId;
    var userId = req.query.userId;
    var db = firebase.database();
    var ref = db.ref("favourite");
    var usersRef = ref.child(userId);

    usersRef.push().set({
        imdbId: imdbId
    })
    
    res.send(200, "Added to favourite");
    res.end();
}

function getFavouritiesList(req, res, next){
    var favouriteList = [];
    console.log("get fav");
    var userId = req.query.userId;
    var location = "favourite/" + userId;
    
    var ref = firebase.database().ref(location);
    
    ref.once("value", function(snapshot) {
        
        snapshot.forEach(function(data){
            favouriteList.push({'imdbId': data.val().imdbId});
        });
        
        favouriteList = {'list': favouriteList};
        console.log(favouriteList.list.length);
        var infoCombinedList = [];
        for(var i=0; i< favouriteList.list.length; i++){
            console.log(favouriteList.list[i].imdbId)
            var movieData = syncRequest('GET', omdburl, {
                    'qs':{
                        'i': favouriteList.list[i].imdbId,
                        'plot': "short",
                        'r': "json"
                    }
                });
            var jsonData = JSON.parse(movieData.getBody('utf-8'));
            var movieTitle = jsonData.Title;
            var movieDes = jsonData.Plot;
            var moviePoster = jsonData.Poster;
            
            infoCombinedList.push({
                         'title': movieTitle,
                         'des': movieDes,
                         'poster': moviePoster
                     });
        }
        res.send(200, {'list': infoCombinedList});
        res.end();
    });
}

//Listen to the port
var port = process.env.PORT || 8080;

server.listen(port, function(error) {
        if (error)
            console.error(error)
        else
            console.log('Port: ' + port)
})